<?php $pageid="contact";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>
<div class="p-contact">
	<div class="c-mv">
		<div class="c-title1">
			<div class="c-title1__ttl">CONTACT</div>
			<div class="c-title1__txt">お問い合わせ</div>
		</div>
	</div>
	<div class="p-contact__block">
		<div class="l-content">
			<div class="p-contact__text">お見積りやお打合せのご依頼など、お気軽にお問い合わせください。<br>また、クリエイターやパートナー企業の募集も行っております。</div>
			<div class="c-form1">
				<form action method="post" class="mailForm">
					<p class="c-form1__label mb"><span>*</span>は必須項目です。</p>
					<div class="c-form1__row">
						<div class="c-form1__ttl">
							<p class="c-form1__label">姓<span>*</span></p>
						</div>
						<div class="c-form1__content"><input type="text"></div>
					</div>
					<div class="c-form1__row">
						<div class="c-form1__ttl">
							<p class="c-form1__label">名<span>*</span></p>
						</div>
						<div class="c-form1__content"><input type="text"></div>
					</div>
					<div class="c-form1__row">
						<div class="c-form1__ttl">
							<p class="c-form1__label">フリガナ</p>
						</div>
						<div class="c-form1__content"><input type="text"></div>
					</div>
					<div class="c-form1__row">
						<div class="c-form1__ttl">
							<p class="c-form1__label">会社名/学校名</p>
						</div>
						<div class="c-form1__content"><input type="text"></div>
					</div>
					<div class="c-form1__row">
						<div class="c-form1__ttl">
							<p class="c-form1__label">部署名</p>
						</div>
						<div class="c-form1__content"><input type="text"></div>
					</div>
					<div class="c-form1__row">
						<div class="c-form1__ttl">
							<p class="c-form1__label">メールアドレス(半角)<span>*</span></p>
						</div>
						<div class="c-form1__content"><input type="text"></div>
					</div>
					<div class="c-form1__row">
						<div class="c-form1__ttl">
							<p class="c-form1__label">URL(半角)</p>
						</div>
						<div class="c-form1__content"><input type="text"></div>
					</div>
					<div class="c-form1__row">
						<div class="c-form1__ttl">
							<p class="c-form1__label">電話番号(半角)<span>*</span></p>
						</div>
						<div class="c-form1__content"><input type="text"></div>
					</div>
					<div class="c-form1__row">
						<div class="c-form1__ttl">
							<p class="c-form1__label">お問い合わせ詳細<span>*</span></p>
						</div>
						<div class="c-form1__content"><textarea></textarea></div>
					</div>
					<div class="c-form1__btn">
						<button><span>送信</span></button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
