<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<?php include('meta.php'); ?>
<link href="/assets/css/style.css" rel="stylesheet">
<link href="/assets/css/slick.css" rel="stylesheet">
<link href="/assets/css/slick-theme.css" rel="stylesheet">
<script src="/assets/js/jquery-3.3.1.min.js"></script>
<script src="/assets/js/jquery-migrate-3.0.1.min.js"></script>
</head>
<body class="page-<?php echo $pageid; ?>">
<header class="c-head">
	<div class="c-head__inner">
		<h1 class="pc-only"><a href="/"><img src="assets/img/common/logo.png"></a></h1>
		<nav class="c-head__navi">
			<div class="c-head__navcon">
				<ul>
					<li><a href="/"><span>TOP</span><small>トップページ</small></a></li>
					<li><a href="/business"><span>BUSINESS</span><small>事業内容</small></a></li>
					<li><a href="/uniqueness"><span>UNIQUENESS</span><small>特徴</small></a></li>
					<li><a href="/works"><span>WORKS</span><small>制作実績</small></a></li>
					<li><a href="/company"><span>COMPANY</span><small>会社概要</small></a></li>
					<li><a href="/contact"><span>CONTACT</span><small>お問い合わせ</small></a></li>
				</ul>
			</div>
		</nav>
	</div>
	<div class="c-headbtn sp-only">
		<div class="c-headbtn__icon">
			<span class="c-headbtn__line"></span>
			<span class="c-headbtn__line"></span>
			<span class="c-headbtn__line"></span>
		</div>
	</div>
</header>
