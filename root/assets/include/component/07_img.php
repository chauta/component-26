<?php /*========================================
img
================================================*/ ?>
<div class="c-dev-title1">img</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-imgtext1</div>
<div class="c-imgtext1">
	<div class="c-imgtext1__img">
		<img src="assets/img/common/img1.jpg" alt="" width="489" height="357" >
	</div>
	<div class="c-imgtext1__txt">
		<div class="c-title1">
			<div class="c-title1__ttl">
				WORKS
			</div>
			<div class="c-title1__txt">
				制作実績
			</div>
		</div>
		<p>私たちは東証一部の企業からベンチャー企業まで、<br>幅広い会社の制作物に携わってきました。<br>お会いする前に、<br>実績の一部をここでお見せいたします。<br></p>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-imgtext2</div>
<div class="l-content">
			<div class="c-imgtext2">
				<div class="c-imgtext2__img">
					<img src="./assets/img/common/imgtxt1.jpg" alt="" width="359" height="172">
				</div>
				<div class="c-imgtext2__content">
					<div class="c-title3">
						<div class="c-title3__ttl">
							Video
						</div>
						<div class="c-title3__txt">
							動画制作
						</div>
					</div>
					<div class="c-imgtext2__text">プレゼンテーションや<br>集客で効果抜群。</div>
				</div>
			</div>
			<div class="c-imgtext2 c-imgtext2--mode2">
				<div class="c-imgtext2__img">
					<img src="./assets/img/common/imgtxt2.jpg" alt="" width="359" height="172">
				</div>
				<div class="c-imgtext2__content">
					<div class="c-title3">
						<div class="c-title3__ttl">
							Website
						</div>
						<div class="c-title3__txt">
							ホームページ制作
						</div>
					</div>
					<div class="c-imgtext2__text">閲覧者が、他もページを<br>読み込みたくなる。</div>
				</div>
			</div>
			<div class="c-imgtext2">
				<div class="c-imgtext2__img">
					<img src="./assets/img/common/imgtxt3.jpg" alt="" width="359" height="172">
				</div>
				<div class="c-imgtext2__content">
					<div class="c-title3">
						<div class="c-title3__ttl">
							Brochure
						</div>
						<div class="c-title3__txt">
							パンフレット制作
						</div>
					</div>
					<div class="c-imgtext2__text">渡された人が<br>一瞬で貴社に興味を持つ。</div>
				</div>
			</div>
			<div class="c-imgtext2 c-imgtext2--mode2">
				<div class="c-imgtext2__img">
					<img src="./assets/img/common/imgtxt4.jpg" alt="" width="359" height="172">
				</div>
				<div class="c-imgtext2__content">
					<div class="c-title3">
						<div class="c-title3__ttl">
							Equipment
						</div>
						<div class="c-title3__txt">
							イベント備品
						</div>
					</div>
					<div class="c-imgtext2__text">歩く人が<br>ブースを訪れたくなる。</div>
				</div>
			</div>
			<div class="c-imgtext2">
				<div class="c-imgtext2__img">
					<img src="./assets/img/common/imgtxt5.jpg" alt="" width="359" height="172">
				</div>
				<div class="c-imgtext2__content">
					<div class="c-title3">
						<div class="c-title3__ttl">
							Comic
						</div>
						<div class="c-title3__txt">
							マンガ制作
						</div>
					</div>
					<div class="c-imgtext2__text">ストーリーで、<br>ターゲットの記憶に残り続ける。</div>
				</div>
			</div>
			<div class="c-imgtext2 c-imgtext2--mode2">
				<div class="c-imgtext2__img">
					<img src="./assets/img/common/imgtxt6.jpg" alt="" width="359" height="172">
				</div>
				<div class="c-imgtext2__content">
					<div class="c-title3">
						<div class="c-title3__ttl">
							Etc
						</div>
						<div class="c-title3__txt">
							その他、何なりと
						</div>
					</div>
					<div class="c-imgtext2__text">ノベルティ制作などを含め、<br>ニーズに合わせて商品を提供。</div>
				</div>
			</div>
		</div>
