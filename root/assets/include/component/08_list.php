<?php /*========================================
list
================================================*/ ?>
<div class="c-dev-title1">list</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-clist1</div>
<div class="l-container">
	<div class="c-list1">
		<div class="c-list1__card">
			<div class="c-list1__img">
				<img src="/assets/img/common/icon1.png" alt="">
			</div>
			<div class="c-title2">
				<div class="c-title2__ttl">Video</div>
				<p class="c-title2__txt">動画制作</p>
			</div>
		</div>
		<div class="c-list1__card">
			<div class="c-list1__img">
				<img src="/assets/img/common/icon2.png" alt="">
			</div>
			<div class="c-title2">
				<div class="c-title2__ttl">Brochure</div>
				<p class="c-title2__txt">パンフレット制作</p>
			</div>
		</div>
		<div class="c-list1__card">
			<div class="c-list1__img">
				<img src="/assets/img/common/icon3.png" alt="">
			</div>
			<div class="c-title2">
				<div class="c-title2__ttl">Website</div>
				<p class="c-title2__txt">ホームページ制作</p>
			</div>
		</div>
		<div class="c-list1__card">
			<div class="c-clist1__img">
				<img src="/assets/img/common/icon4.png" alt="">
			</div>
			<div class="c-title2">
				<div class="c-title2__ttl">Comic</div>
				<p class="c-title2__txt">マンガ制作</p>
			</div>
		</div>
		<div class="c-list1__card">
			<div class="c-clist1__img">
				<img src="/assets/img/common/icon5.png" alt="">
			</div>
			<div class="c-title2">
				<div class="c-title2__ttl">Equipment</div>
				<p class="c-title2__txt">イベント備品</p>
			</div>
		</div>
		<div class="c-list1__card">
			<div class="c-clist1__img">
				<img src="/assets/img/common/icon6.png" alt="">
			</div>
			<div class="c-title2">
				<div class="c-title2__ttl">Etc</div>
				<p class="c-title2__txt">その他、何なりと</p>
			</div>
		</div>
	</div>
</div>

