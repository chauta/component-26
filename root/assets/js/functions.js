
$(function(){
	$('.c-headbtn').on('click',function(){
		$(this).toggleClass('is-active');
		$('.c-head__navi').toggleClass('is-open');
		if ($(this).hasClass("is-active")) {
			$('html,body').css('overflow-y', 'hidden');
		} else {
			$('html').css('overflow-y', 'scroll');
			$('body').css('overflow-y', 'auto');
		}
	});
});

$(function () {
	$('.c-slide1').slick({
		cssEase: 'linear',
		variableWidth: true,
		infinite: true,
		arrows: false,
		dots: false,
 		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 3000,
		responsive:[
			{
				breakpoint:768,
				settings:{
					variableWidth: false,
				}
			}
		]
	});
});