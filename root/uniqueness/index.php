<?php $pageid="uniqueness";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>
<div class="p-uniqueness">
	<div class="c-mv">
		<div class="c-title1">
			<div class="c-title1__ttl">UNIQUENESS</div>
			<div class="c-title1__txt">特徴S</div>
		</div>
	</div>
	<div class="p-uniqueness">
		<div class="l-container">
			<div class="c-imgtext3">
				<div class="c-imgtext3__img">
					<img src="./assets/img/common/img3.jpg" width="403" height="322">
				</div>
				<div class="c-imgtext3__content">
					<div class="c-imgtext3__ttl">貴社に二つの「楽」を与える<br>制作物を作れます</div>
					<div class="c-imgtext3__txt">クオリティが高く、価格が安い。<br>これはほとんどの制作会社様がアピールされることであり、<br>そしてほとんどのお客様が求めることであるはずです。<br>弊社ももちろん例外ではありません。<br>「本当にこの価格で良いのか」と驚かれるお客様も多いです。<br>ただし、私たちが他にも大切にしているのは、<br>貴社、そしてあなたに「楽」を提供することです。<br>そしてこの「楽」には、二つの意味があると考えています。</div>
				</div>
			</div>
		</div>
		<div class="c-imgbg mt2">
				<div class="c-title6">
					<div class="c-title6__img">
						<img src="./assets/img/common/title1.png" width="164" height="49">
					</div>
				</div>
				<div class="c-imgbg__tl">
					<p>作るのも、見せるのも楽しくなる</p>
				</div>
				<div class="c-imgbg__txt">
					<p>みなさんが作っていて楽しくなる、そして制作物を見せるのも楽しくなるような仕組みづくりを心がけています。制作物、いわゆるクリエイティブ商材は、本来楽しいものでなければなりません。<br>制作物は、ユーザーに購入・応募といった行動を起こしてもらうためのものです。そしてそれは、ユーザーに少なからず楽しみを感じてもらって実現するものです。だからこそ、その制作物を作るにあたって私たちやお客様が楽しむのは当然とも言えます。<br>また、他と一線を画す完成物を、お客様がついつい他の人に見せたくなるようになってほしいと、強く思っています。だからこそ、制作チームはモノづくりを心から好きでやっている人材を集めております。私たちは、他とは違うものを考えるスペシャリスト集団です。エンターティナーたちによる企画・提案にぜひご期待ください。</p>
				</div>
			</div>
			<div class="c-imgbg c-imgbg--mode2 mt3">
				<div class="c-title6">
					<div class="c-title6__img">
						<img src="./assets/img/common/title2.png" width="164" height="49">
					</div>
				</div>
				<div class="c-imgbg__tl">
					<p>仕事が一気に楽になる</p>
				</div>
				<div class="c-imgbg__txt">
					<p>もはや制作物に限らず、サービスを導入するということは本質的に「自分では手がかかることを他者にゆだねる」であるはずです。私たちはその本質をとても大切にし、お客様のお仕事をいかに減らせるかを常に考えています。<br>制作物の場合であれば、動画を上映することでプレゼンや説明会の工数を削減する、ホームページ経由の問い合わせを増やすことで営業の工数を削減する、などが考えられます。効果が出るのは大前提ですが、その効果を出す目的を忘れずに制作に取り組ませていただいております。<br>制作におけるやり取りでも同様です。お客様が余分にやり取りをする必要がないよう、提案のタイミング・方法の効率化を意識しています。前述の楽しさに加えて合理化にもこだわり、制作過程から納品後の活用まで、お客様が楽をできるようにやり取りさせていただきます。</p>
				</div>
			</div>
			<div class="c-contact mt">
				<div class="c-title4">
					<div class="c-title4__ttl">
						CONTACT
					</div>
					<div class="c-title4__txt">
						お問い合わせ
					</div>
				</div>
				<p class="c-contact__text">お見積りやお打合せのご依頼など、お気軽にお問い合わせください。<br>また、クリエイターやパートナー企業の募集も行っております。
				</p>
				<div class="c-btn1">
					<a href="">FORM</a>
				</div>
			</div>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
