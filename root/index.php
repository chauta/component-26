<?php $pageid="index";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>
<div class="p-top">
	<div class="p-top__block1">
		<div class="pc-only">
			<img src="assets/img/common/ms-text.svg" width="793" height="113" >
		</div>
		<div class="sp-only">
			<img src="assets/img/common/ms-text-sp.svg" width="265" height="170">
		</div>
	</div>
	<div class="p-top__block2">
		<div class="l-container">
			<div class="c-title1">
				<div class="c-title1__ttl">
					Our Business
				</div>
				<div class="c-title1__txt">
					事業内容
				</div>
			</div>
			<div class="c-list1 mr">
				<div class="c-list1__card">
					<div class="c-list1__img">
						<img src="/assets/img/common/icon1.png" alt="">
					</div>
					<div class="c-title2">
						<div class="c-title2__ttl">Video</div>
						<p class="c-title2__txt">動画制作</p>
					</div>
				</div>
				<div class="c-list1__card">
					<div class="c-list1__img">
						<img src="/assets/img/common/icon2.png" alt="">
					</div>
					<div class="c-title2">
						<div class="c-title2__ttl">Brochure</div>
						<p class="c-title2__txt">パンフレット制作</p>
					</div>
				</div>
				<div class="c-list1__card">
					<div class="c-list1__img">
						<img src="/assets/img/common/icon3.png" alt="">
					</div>
					<div class="c-title2">
						<div class="c-title2__ttl">Website</div>
						<p class="c-title2__txt">ホームページ制作</p>
					</div>
				</div>
				<div class="c-list1__card">
					<div class="c-list1__img">
						<img src="/assets/img/common/icon4.png" alt="">
					</div>
					<div class="c-title2">
						<div class="c-title2__ttl">Comic</div>
						<p class="c-title2__txt">マンガ制作</p>
					</div>
				</div>
				<div class="c-list1__card">
					<div class="c-list1__img">
						<img src="/assets/img/common/icon5.png" alt="">
					</div>
					<div class="c-title2">
						<div class="c-title2__ttl">Equipment</div>
						<p class="c-title2__txt">イベント備品</p>
					</div>
				</div>
				<div class="c-list1__card">
					<div class="c-list1__img">
						<img src="/assets/img/common/icon6.png" alt="">
					</div>
					<div class="c-title2">
						<div class="c-title2__ttl">Etc</div>
						<p class="c-title2__txt">その他、何なりと</p>
					</div>
				</div>
			</div>
		</div>
		<div class="c-btn1">
			<a href="">More</a>
		</div>
	</div>
	<div class="p-top__block3">
		<div class="c-title1 mb">
			<div class="c-title1__ttl">UNIQUENESS</div>
			<div class="c-title1__txt">特徴</div>
		</div>
		<p class="c-text1">一味どころか、百味違うものを</p>
		<p class="c-text2 mr">リーズナブルな金額で、クオリティが高いものを作る。<br>今となっては、それができる企業は少なくありません。<br>し、そこからさらに付加価値を提供できる会社はとても希少です。<br>そのプラスアルファを実現するのが、私たちの使命だと思っています。</p>
		<div class="c-btn1">
			<a href="">More</a>
		</div>
	</div>
	<div class="p-top__block4">
		<div class="l-container">
			<div class="c-imgtext1">
				<div class="c-imgtext1__img">
					<img src="assets/img/common/img1.jpg" alt="" width="489" height="357" >
				</div>
				<div class="c-imgtext1__txt">
					<div class="c-title1  c-title1--mode2">
						<div class="c-title1__ttl">
							WORKS
						</div>
						<div class="c-title1__txt">
							制作実績
						</div>
					</div>
					<p class="c-imgtext1__text">私たちは東証一部の企業からベンチャー企業まで、<br>幅広い会社の制作物に携わってきました。<br>お会いする前に、<br>実績の一部をここでお見せいたします。<br></p>
					<div class="c-btn1 c-btn1--mode2">
						<a href="">More</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
