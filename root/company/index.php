<?php $pageid="company";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>
<div class="p-company">
	<div class="c-mv">
		<div class="c-title1">
			<div class="c-title1__ttl">COMPANY</div>
			<div class="c-title1__txt">会社概要</div>
		</div>
	</div>
	<div class="p-company__block">
		<div class="l-content">
			<div class="c-table1">
				<table class="demo03">
					<tbody>
						<tr>
							<th>社名</th>
							<td>ブリッジ・ジャパン株式会社</td>
						</tr>
						<tr>
							<th>住所</th>
							<td>本社：大阪市中央区南船場四丁目4番21号<br>東京支社：サンプル</td>
						</tr>
						<tr>
							<th>連絡先</th>
							<td>TEL：000-0000-0000　FAX：000-0000-0000</td>
						</tr>
						<tr>
							<th>資本金</th>
							<td>2000万円</td>
						</tr>
						<tr>
							<th>代表取締役</th>
							<td>甲良　壮一</td>
						</tr>
						<tr>
							<th>従業員数</th>
							<td>20名</td>
						</tr>
						<tr>
							<th>事業内容</th>
							<td>動画、ホームページ、パンフレットなどの制作物の制作</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>			
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
