<?php $pageid="business";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>
<div class="p-business">
	<div class="c-mv">
		<div class="c-title1">
			<div class="c-title1__ttl">
				Our Business
			</div>
			<div class="c-title1__txt">
				事業内容
			</div>
		</div>
	</div>
	<div class="p-block1">
		<div class="l-wrap">
			<div class="p-block1__img">
				<img src="./assets/img/common/img2.jpg" alt="" width="917" height="318">
			</div>
			<div class="p-block1__text">
				<div class="p-block1__ttl">広報・採用で活躍する、ハイセンスな制作物</div>
				<div class="c-text2 u-black">以前は「あれば便利」と言われていた制作物。<br>しかし昨今では「持っているのが当たり前」な時代になりつつあると言えます。<br>周りのほとんどが持っているからこそ、他と差をつけなければなりません。<br>その「差」を、私たちが付加価値として提供します。私たちの制作物が、<br>貴社の広報や採用において大きな武器となることをお約束します。<br>
				</div>
			</div>
		</div>
	</div>
	<div class="p-block2">
		<div class="l-content">
			<div class="c-imgtext2">
				<div class="c-imgtext2__img">
					<img src="./assets/img/common/imgtxt1.jpg" alt="" width="359" height="172">
				</div>
				<div class="c-imgtext2__content">
					<div class="c-title3">
						<div class="c-title3__ttl">
							Video
						</div>
						<div class="c-title3__txt">
							動画制作
						</div>
					</div>
					<div class="c-imgtext2__text">プレゼンテーションや<br>集客で効果抜群。</div>
				</div>
			</div>
			<div class="c-imgtext2 c-imgtext2--mode2">
				<div class="c-imgtext2__img">
					<img src="./assets/img/common/imgtxt2.jpg" alt="" width="359" height="172">
				</div>
				<div class="c-imgtext2__content">
					<div class="c-title3">
						<div class="c-title3__ttl">
							Website
						</div>
						<div class="c-title3__txt">
							ホームページ制作
						</div>
					</div>
					<div class="c-imgtext2__text">閲覧者が、他もページを<br>読み込みたくなる。</div>
				</div>
			</div>
			<div class="c-imgtext2">
				<div class="c-imgtext2__img">
					<img src="./assets/img/common/imgtxt3.jpg" alt="" width="359" height="172">
				</div>
				<div class="c-imgtext2__content">
					<div class="c-title3">
						<div class="c-title3__ttl">
							Brochure
						</div>
						<div class="c-title3__txt">
							パンフレット制作
						</div>
					</div>
					<div class="c-imgtext2__text">渡された人が<br>一瞬で貴社に興味を持つ。</div>
				</div>
			</div>
			<div class="c-imgtext2 c-imgtext2--mode2">
				<div class="c-imgtext2__img">
					<img src="./assets/img/common/imgtxt4.jpg" alt="" width="359" height="172">
				</div>
				<div class="c-imgtext2__content">
					<div class="c-title3">
						<div class="c-title3__ttl">
							Equipment
						</div>
						<div class="c-title3__txt">
							イベント備品
						</div>
					</div>
					<div class="c-imgtext2__text">歩く人が<br>ブースを訪れたくなる。</div>
				</div>
			</div>
			<div class="c-imgtext2">
				<div class="c-imgtext2__img">
					<img src="./assets/img/common/imgtxt5.jpg" alt="" width="359" height="172">
				</div>
				<div class="c-imgtext2__content">
					<div class="c-title3">
						<div class="c-title3__ttl">
							Comic
						</div>
						<div class="c-title3__txt">
							マンガ制作
						</div>
					</div>
					<div class="c-imgtext2__text">ストーリーで、<br>ターゲットの記憶に残り続ける。</div>
				</div>
			</div>
			<div class="c-imgtext2 c-imgtext2--mode2">
				<div class="c-imgtext2__img">
					<img src="./assets/img/common/imgtxt6.jpg" alt="" width="359" height="172">
				</div>
				<div class="c-imgtext2__content">
					<div class="c-title3">
						<div class="c-title3__ttl">
							Etc
						</div>
						<div class="c-title3__txt">
							その他、何なりと
						</div>
					</div>
					<div class="c-imgtext2__text">ノベルティ制作などを含め、<br>ニーズに合わせて商品を提供。</div>
				</div>
			</div>
		</div>
	</div>
	<div class="c-contact mb">
		<div class="c-title4">
			<div class="c-title4__ttl">
				CONTACT
			</div>
			<div class="c-title4__txt">
				お問い合わせ
			</div>
		</div>
		<p class="c-contact__text">お見積りやお打合せのご依頼など、お気軽にお問い合わせください。<br>また、クリエイターやパートナー企業の募集も行っております。
		</p>
		<div class="c-btn1">
			<a href="">FORM</a>
		</div>
	</div>
</div>


<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>