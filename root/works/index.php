<?php $pageid="works";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>
<div class="p-works">
	<div class="c-mv">
		<div class="c-title1">
			<div class="c-title1__ttl">WORKS</div>
			<div class="c-title1__txt">制作実績</div>
		</div>
	</div>
	<div class="p-works__block">
		<div class="l-wrapper">
			<div class="c-title5 mr">
				<div class="c-title5__icon">
					<img src="./assets/img/common/icon7.png" alt="" width="57" height="50" >
				</div>
				<div class="c-title5__txt">動画制作実績</div>
			</div>
			<div class="c-slide1">
				<div class="c-slide1__item">
					<div class="c-slide1__visual">
						<iframe width="219" height="147" src="https://www.youtube.com/embed/wAOj6YhMjNA" frameborder="0" allowfullscreen></iframe>
					</div>
					<div class="c-slide1__content">
						<div class="c-slide1__ttl">動画タイトル</div>
						<div class="c-slide1__text">説明文説明文説明文</div>
						<div class="c-slide1__sub">動画URLhttps://www.youtube.com/watch?v=wAOj6YhMjNA</div>
					</div>
				</div>
				<div class="c-slide1__item">
					<div class="c-slide1__visual">
						<iframe width="219" height="147" src="https://www.youtube.com/embed/wAOj6YhMjNA" frameborder="0" allowfullscreen></iframe>
					</div>
					<div class="c-slide1__content">
						<div class="c-slide1__ttl">動画タイトル</div>
						<div class="c-slide1__text">説明文説明文説明文</div>
						<div class="c-slide1__sub">動画URLhttps://www.youtube.com/watch?v=wAOj6YhMjNA</div>
					</div>
				</div>
				<div class="c-slide1__item">
					<div class="c-slide1__visual">
						<iframe width="219" height="147" src="https://www.youtube.com/embed/wAOj6YhMjNA" frameborder="0" allowfullscreen></iframe>
					</div>
					<div class="c-slide1__content">
						<div class="c-slide1__ttl">動画タイトル</div>
						<div class="c-slide1__text">説明文説明文説明文</div>
						<div class="c-slide1__sub">動画URLhttps://www.youtube.com/watch?v=wAOj6YhMjNA</div>
					</div>
				</div>
				<div class="c-slide1__item">
					<div class="c-slide1__visual">
						<iframe width="219" height="147" src="https://www.youtube.com/embed/wAOj6YhMjNA" frameborder="0" allowfullscreen></iframe>
					</div>
					<div class="c-slide1__content">
						<div class="c-slide1__ttl">動画タイトル</div>
						<div class="c-slide1__text">説明文説明文説明文</div>
						<div class="c-slide1__sub">動画URLhttps://www.youtube.com/watch?v=wAOj6YhMjNA</div>
					</div>
				</div>
			</div>
			<div class="c-title5 mr">
				<div class="c-title5__icon">
					<img src="./assets/img/common/icon8.png" alt="" width="57" height="50" >
				</div>
				<div class="c-title5__txt">パンフレット制作</div>
			</div>
			<div class="c-slide1">
				<div class="c-slide1__item">
					<div class="c-slide1__visual">
						<img src="./assets/img/common/sample.jpg" alt="" width="219" height="147">
					</div>
					<div class="c-slide1__content">
						<div class="c-slide1__ttl">動画タイトル</div>
						<div class="c-slide1__text">説明文説明文説明文</div>
					</div>
				</div>
				<div class="c-slide1__item">
					<div class="c-slide1__visual">
						<img src="./assets/img/common/sample.jpg" alt="" width="219" height="147">
					</div>
					<div class="c-slide1__content">
						<div class="c-slide1__ttl">動画タイトル</div>
						<div class="c-slide1__text">説明文説明文説明文</div>
					</div>
				</div>
				<div class="c-slide1__item">
					<div class="c-slide1__visual">
						<img src="./assets/img/common/sample.jpg" alt="" width="219" height="147">
					</div>
					<div class="c-slide1__content">
						<div class="c-slide1__ttl">動画タイトル</div>
						<div class="c-slide1__text">説明文説明文説明文</div>
					</div>
				</div>
				<div class="c-slide1__item">
					<div class="c-slide1__visual">
						<img src="./assets/img/common/sample.jpg" alt="" width="219" height="147">
					</div>
					<div class="c-slide1__content">
						<div class="c-slide1__ttl">動画タイトル</div>
						<div class="c-slide1__text">説明文説明文説明文</div>
					</div>
				</div>
				<div class="c-slide1__item">
					<div class="c-slide1__visual">
						<img src="./assets/img/common/sample.jpg" alt="" width="219" height="147">
					</div>
					<div class="c-slide1__content">
						<div class="c-slide1__ttl">動画タイトル</div>
						<div class="c-slide1__text">説明文説明文説明文</div>
					</div>
				</div>
			</div>
			<div class="c-title5 mr">
				<div class="c-title5__icon">
					<img src="./assets/img/common/icon9.png" alt="" width="57" height="50" >
				</div>
				<div class="c-title5__txt">ホームページ</div>
			</div>
			<div class="c-slide1">
				<div class="c-slide1__item">
					<div class="c-slide1__visual">
						<iframe width="219" height="147" src="https://www.youtube.com/embed/wAOj6YhMjNA" frameborder="0" allowfullscreen></iframe>
					</div>
					<div class="c-slide1__content">
						<div class="c-slide1__ttl">動画タイトル</div>
						<div class="c-slide1__text">説明文説明文説明文</div>
						<div class="c-slide1__sub">動画URLhttps://www.youtube.com/watch?v=wAOj6YhMjNA	</div>
					</div>
				</div>
				<div class="c-slide1__item">
					<div class="c-slide1__visual">
						<iframe width="219" height="147" src="https://www.youtube.com/embed/wAOj6YhMjNA" frameborder="0" allowfullscreen></iframe>
					</div>
					<div class="c-slide1__content">
						<div class="c-slide1__ttl">動画タイトル</div>
						<div class="c-slide1__text">説明文説明文説明文</div>
						<div class="c-slide1__sub">動画URLhttps://www.youtube.com/watch?v=wAOj6YhMjNA	</div>
					</div>
				</div>
				<div class="c-slide1__item">
					<div class="c-slide1__visual">
						<iframe width="219" height="147" src="https://www.youtube.com/embed/wAOj6YhMjNA" frameborder="0" allowfullscreen></iframe>
					</div>
					<div class="c-slide1__content">
						<div class="c-slide1__ttl">動画タイトル</div>
						<div class="c-slide1__text">説明文説明文説明文</div>
						<div class="c-slide1__sub">動画URLhttps://www.youtube.com/watch?v=wAOj6YhMjNA	</div>
					</div>
				</div>
				<div class="c-slide1__item">
					<div class="c-slide1__visual">
						<iframe width="219" height="147" src="https://www.youtube.com/embed/wAOj6YhMjNA" frameborder="0" allowfullscreen></iframe>
					</div>
					<div class="c-slide1__content">
						<div class="c-slide1__ttl">動画タイトル</div>
						<div class="c-slide1__text">説明文説明文説明文</div>
						<div class="c-slide1__sub">動画URLhttps://www.youtube.com/watch?v=wAOj6YhMjNA	</div>
					</div>
				</div>
				<div class="c-slide1__item">
					<div class="c-slide1__visual">
						<iframe width="219" height="147" src="https://www.youtube.com/embed/wAOj6YhMjNA" frameborder="0" allowfullscreen></iframe>
					</div>
					<div class="c-slide1__content">
						<div class="c-slide1__ttl">動画タイトル</div>
						<div class="c-slide1__text">説明文説明文説明文</div>
						<div class="c-slide1__sub">動画URLhttps://www.youtube.com/watch?v=wAOj6YhMjNA	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
